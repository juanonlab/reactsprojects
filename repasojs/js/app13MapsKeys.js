
// Maps y keys
const carrito = ['Producto1', 'Producto2', 'Producto3'];

console.log(carrito);

const appContenedor = document.querySelector('#app');

let html = '';
carrito.forEach(producto => {
    html += `<li> ${producto} </li>`;
})
appContenedor.innerHTML = html;


// No muta el array original
// Muy útil para procesar los objetos de la lista
const carrito2 = ['Producto1', 'Producto2', 'Producto3'];
const carritoMap = carrito2.map(producto => {
    return `El producto es ${producto}`;
})
console.log(carritoMap);

// Multipicar por 2 los siguientes números 
const numeros = [2, 4, 8];
const numerosDobles = numeros.map(numeroAMultiplicar => {
    return `${numeroAMultiplicar}`*2;
})
console.log(numerosDobles);


// Object keys

const persona = {
    nombre: 'Juan',
    profesion: 'Desarrollador web',
    edad: 40
}

const {nombre} = persona;
console.log(nombre);

console.log(persona);

// Devuelve los campos(keys) no el contenido
console.log(Object.keys(persona)); 