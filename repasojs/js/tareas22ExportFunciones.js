


export const nombreTarea = 'Pasear al perro';

export const crearTarea = (tarea, urgencia) => {
    return `La tarea ${tarea} tiene una urgencia de ${urgencia}`;
}

export const tareaCompletada = () => {
    return `La tarea se ha completado`;
}


class Tarea{

    constructor(nombre, prioridad){

        this.nombre = nombre;
        this.prioridad = prioridad;
    }

    mostrar(){
        return(`${this.nombre} tiene una prioridad de ${this.prioridad}`);
    }

}

class ComprasPendientes extends Tarea {
    constructor(nombre, prioridad, cantidad) {
        super(nombre, prioridad);
        this.cantidad = cantidad;
    }

    // Puedes tener otra funcion mostrar
    mostrar(){
        let valor = super.mostrar();
        return `${valor} y la cantidad de ${this.cantidad}`;
    }

}