// Objetos

// Object literal
const persona = {
    nombre: 'Juan',
    profesion: 'Desarrollador web',
    edad: 500
}

// let nombrePersona = 'Juan';
// let profesion = 'Desarollador web';
// console.log(nombrePersona);
// console.log(profesion);

console.log(persona);

console.log(persona.nombre);
console.log(persona.profesion);
console.log(persona.edad);

console.log(persona['nombre']);
console.log(persona['profesion']);
console.log(persona['edad']);