
// Funcion tradicional
// let viajando = function(destino) {
//     return `Viajando a la ciudad de : ${destino}`;
// }

// Arrow functions
// La palabra function pasa a la derecha de la declaración
// y se transforma en =>

// 1 parametro
let viajando = destino => {
    return `Viajando a la ciudad de : ${destino}`;
}

// 2 parametros
let viajandoMucho = (destino, horas) => {
    return `Viajando a la ciudad de ${destino} durante ${horas} horas.`;
}

// 1 parametro simplificado
let viajandoNormal = destino => `Viajando a la ciudad de : ${destino}`;


let viaje;

viaje = viajando('Paris');
console.log(viaje);

viaje = viajando('Madrid');
console.log(viaje);

viaje = viajandoMucho('Creta',12);
console.log(viaje);

viaje = viajandoNormal('Japon');
console.log(viaje);


