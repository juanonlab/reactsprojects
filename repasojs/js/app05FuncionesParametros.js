
// Parametros por default en las funciones
// function actividad(nombre = 'Perico' , actividad = 'Enseña Mates'){
//     console.log(`La persona ${nombre} está realizando la actividad ${actividad}`);
// }

const actividad = function (nombre = 'Perico' , actividad = 'Enseña Mates'){
    console.log(`La persona ${nombre} está realizando la actividad ${actividad}`);
}

// Llamada a la funcion
actividad ('Juan', 'Aprender js');

// No falla el método pero sale undefined puedes
// añadir por defecto Perico y Enseñá Mates
actividad(); 

// Esta llamada muestra: La persona Antonio está realizando la actividad Enseña Mates
actividad('Antonio');
