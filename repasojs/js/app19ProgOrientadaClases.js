// Escribir classes

class Tarea{

    constructor(nombre, prioridad){

        this.nombre = nombre;
        this.prioridad = prioridad;
    }

    mostrar(){
        return(`${this.nombre} tiene una prioridad de ${this.prioridad}`);
    }

}



// Crear los objetos
let tarea1 = new Tarea('Aprender JS', 'Alta');
let tarea2 = new Tarea('Preparar café', 'Baja');
let tarea3 = new Tarea('Conocer Suegros', 'Media');
let tarea4 = new Tarea('Pasear Perro', 'Alta');

// Llamar a la función
console.log(tarea1.mostrar());
console.log(tarea2.mostrar());
console.log(tarea3.mostrar());
console.log(tarea4.mostrar());

