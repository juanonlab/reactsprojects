// Destructuring de objeto
// Obtener la info de un objeto

const aprendiendoJS = {
    version : {
        nueva : 'ES6',
        anterior : 'ES5'
    },
    frameworks: ['React','VueJS','AngularJS']
}

// Destructuring antiguo
console.log(aprendiendoJS);
let versionA = aprendiendoJS.version.nueva;
let frameworkA = aprendiendoJS.frameworks[1];
console.log(versionA);
console.log(frameworkA);


// Destructuring nuevo es extraer valoresde un objeto
let {version, frameworks} = aprendiendoJS;
console.log(version);
console.log(frameworks);

// Destructuring es extraer valoresde un objeto
// Tienes que saber a donde ir ojo!!
// al hacer aprendiendoJS.version js sabe que solo tiene nueva y anterior como variables
// Todo lo demás va a petarrrrr!!
let {nueva, anterior} = aprendiendoJS.version;
console.log(nueva);
console.log(anterior);