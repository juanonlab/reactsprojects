


// Se puede llamar antes de definirlo.
// Primero se leen todas las funciones
saludar('Perico');

// Function declaration
function saludar(nombre){
    console.log(`Bienvenido ${nombre}.`);
}

// Llamadas function declaration
saludar('Juan');
saludar('Virginia');
saludar('Miguel');


// Debes declarar primero las function expression
//cliente('Madoka'); // Error!! Error!! Error!!

// Function expression
const cliente = function(nombreCliente){
    console.log(`Mostrando datos del cliente: ${nombreCliente}.`);
}

// Llamadas function expression
cliente('Juan');
cliente('Pedro');

// La principal diferencia es que debes declarar primero
// la funcion en las expression, antes de llamarlo.