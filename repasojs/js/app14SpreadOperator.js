// Spread operator ...
// Combinar arreglos por ejemplo
// Genera una copia del array
// Mejora los pasos de parámetros

let lenguajes = ['Javascript','PHP','Python'];
let frameworks = ['Reactjs','Laravel','Django'];

// Antiguo
let combinacionA = lenguajes.concat(frameworks);
console.log(combinacionA);

// Moderno
let combinacion = [...lenguajes,...frameworks];
console.log(combinacion);

let nuevo = [...lenguajes];
console.log(nuevo);

// El método nuevo genera una copia del array, es mejor que
// el metodo antiguo que queda afectado y se queda al revés
let lenguajesT1 = ['Javascript','PHP','Python'];
let[ultimoT1] = lenguajesT1.reverse();

console.log(lenguajesT1);
console.log(ultimoT1);

let lenguajesOk = ['Javascript','PHP','Python'];
let[ultimoOk] = [...lenguajesOk].reverse();

console.log(lenguajesOk);
console.log(ultimoOk);

// Mejora el paso de parametros
function suma(a, b, c){

    console.log(a+b+c);
}
const numeros = [1,2,3];
suma(numeros) // Da error
suma(...numeros); // Funciona