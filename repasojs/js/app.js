
// Importando clases
import Tarea from './tareas.js';
import ComprasPendientes from './compras.js';


const tarea1 = new Tarea('Aprender JS', 'Urgente');

console.log(tarea1);
tarea1.mostrar();


const compra1 = new ComprasPendientes('Pizza', 'Urgente',2);
console.log(compra1);
let salida = compra1.mostrar();

console.log(salida);


