// Constantes para el ejemplo
const nombre = 'Juan';
const trabajo = 'Desarrollador web';

// Concatenar variables (antigua)
console.log('Nombre: ' + nombre + ', Trabajo: ' + trabajo);

// Concatenar nueva forma
console.log(`Nombre: ${nombre}, Trabajo: ${trabajo}`);

// Recuperado el elemento #app
const contenedorApp = document.querySelector('#app');

// Concatenar con múltiples lineas (Modo antiguo)
// let html = '<ul>' +
//                 '<li> Nombre: ' + nombre + '</li>' +
//                 '<li> Trabajo: ' + trabajo + '</li>' +
//             '</ul>';

// Concatenar con múltiples lineas (Modo moderno)
let html = `
        <ul>
            <li> Nombre: ${nombre} </li>
            <li> Trabajo: ${trabajo} </li>
        </ul>
`;

// Pasar el valos a la página
contenedorApp.innerHTML = html;            
