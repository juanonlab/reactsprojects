
// Crear variables de manera antigua
var aprendiendoJs;
//aprendiendoJs = false;
//aprendiendoJs = 20;
aprendiendoJs = 'Me gusta js con var';
if (console) {
    console.log(aprendiendoJs);
}


// Variables con const (Debe tener valor por defecto)
// Y nunca cambian
const aprendiendo = 'Javascript';
// aprendiendo = 20; Error
if (console) {
    console.log(aprendiendo);
}


// Variables let, similar a var
let aprendiendoJsLet;
//aprendiendoJsLet = false;
//aprendiendoJsLet = 20;
aprendiendoJsLet = 'Me gusta js con let.';
//aprendiendoJsLet = false;
if (console) {
    console.log(aprendiendoJsLet);
}

