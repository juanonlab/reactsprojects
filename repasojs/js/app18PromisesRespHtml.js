
const descargarUsuarios = cantidad => new Promise((resolve, reject) => {

    const api = `https://randomuser.me/api/?results=${cantidad}`;

    // Llamada a ajax
    const xhr = new XMLHttpRequest();

    // Abrir conexion
    xhr.open('GET', api, true); // true -> asinc

    xhr.onload= () => {
        if (xhr.status === 200) {
            resolve(JSON.parse(xhr.responseText).results);
        } else {
            reject(Error(xhr.statusText));
        }
    }

    // opcional
    xhr.onerror = (error) => reject(error);

    // Envio
    xhr.send();

});

descargarUsuarios(10).then(
    miembros => imprimirHTML(miembros),
    error => console.error(
        new Error(`Hubo un error: ${error}`)
    )
)

function imprimirHTML(usuarios) {
    let html = '';
    usuarios.forEach(usuario => {
        html += `
        <li>
            Nombre: ${usuario.name.first} ${usuario.name.last}
            País: ${usuario.nat}
            Imagen: 
                <img src="${usuario.picture.medium}">
        </li>    
        `;
    });

    const contenedorApp = document.querySelector('#app');
    contenedorApp.innerHTML = html;
}