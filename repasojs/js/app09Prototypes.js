// Objetos


function mostrarInformacionTarea(tarea, prioridad){
    return `La tarea ${tarea} tiene una prioridad de ${prioridad}.`;
   }
   
   // Object literal
   const persona = {
       nombre: 'Juan',
       profesion: 'Desarrollador web',
       edad: 500
   }
   
   
   // const mostrarCliente = mostrarInformacionTarea(persona.nombre, persona.prfesion);
   // console.log(mostrarCliente);
   
   // Object constructor (En mayúsculas)
   function Tarea(nombre, urgencia){
       this.nombre = nombre;
       this.urgencia = urgencia;
   }
   
   // Prototype unido a Tarea (IMP: Atar funciones a objetos)
   // Tus métodos sólo van para objetos tarea
   Tarea.prototype.mostrarInformacionTarea = function(){
       return `La tarea ${this.nombre} tiene una prioridad de ${this.urgencia}.`;
   }
   
   // Se utiliza menos que el literal pq js soporte clases
   const tarea1 = new Tarea('Jugar al fútbol','Muy urgente');
   const tarea2 = new Tarea('Limpiar la casa','Poco urgente');
   console.log(tarea1);
   console.log(tarea1.mostrarInformacionTarea());
   console.log(tarea2);
   console.log(tarea2.mostrarInformacionTarea());
   
   