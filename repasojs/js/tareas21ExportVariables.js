

const persona = 'Es simpatico';
const nombreTarea = 'Pasear perro';

// 1 export default por documento

//export default nombreTarea; 

export default  {
    nombre : nombreTarea,
    persona : persona
}

export const nombreLlave = 'Voy con llaves jo';


class Tarea{

    constructor(nombre, prioridad){

        this.nombre = nombre;
        this.prioridad = prioridad;
    }

    mostrar(){
        return(`${this.nombre} tiene una prioridad de ${this.prioridad}`);
    }

}

class ComprasPendientes extends Tarea {
    constructor(nombre, prioridad, cantidad) {
        super(nombre, prioridad);
        this.cantidad = cantidad;
    }

    // Puedes tener otra funcion mostrar
    mostrar(){
        let valor = super.mostrar();
        return `${valor} y la cantidad de ${this.cantidad}`;
    }

}