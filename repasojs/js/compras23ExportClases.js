class ComprasPendientes extends Tarea {
    constructor(nombre, prioridad, cantidad) {
        super(nombre, prioridad);
        this.cantidad = cantidad;
    }

    // Puedes tener otra funcion mostrar
    mostrar(){
        let valor = super.mostrar();
        return `${valor} y la cantidad de ${this.cantidad}`;
    }

}