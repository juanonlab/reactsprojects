// Objetos

// Object literal
const persona = {
    nombre: 'Juan',
    profesion: 'Desarrollador web',
    edad: 500
}

console.log(persona);

// Object constructor (En mayúsculas)
function Tarea(nombre, urgencia){
    this.nombre = nombre;
    this.urgencia = urgencia;
}

// Se utiliza menos que el literal pq js soporte clases
const tarea1 = new Tarea('Jugar al fútbol','Muy urgente');
const tarea2 = new Tarea('Dormir','No urgente');
console.log(tarea1);
console.log(tarea2);
