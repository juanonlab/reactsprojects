// Scope con Var
// var musica = 'Rock';
// if (musica) {
//      var musica = 'Grunge';
//      console.log('Dentro del If:',musica);
// }
// console.log('Fuera del If:', musica);
// Imprime Grunge y Grunge

// Scope con Let
let musica = 'Rock';
if (musica) {
    let musica = 'Grunge';
    console.log('Dentro del If:', musica);
}
console.log('Fuera del If:', musica);
// Imprime Grunge y Rock

// Scope con const
// const musica = 'Rock';
// if (musica) {
//      const musica = 'Grunge';
//      console.log('Dentro del If:',musica);
// }
// console.log('Fuera del If:', musica);
// Imprime Grunge y Rock
// La const no falla porque está en 2 bloques distintos (entre llaves)