// Object Literal enhancement

const banda = 'Metallica';
const genero = 'Heavy Metal';
const canciones = ['Cancion1','Cancion2','Cancion3'];

// Forma antigua :(
const metallicaV = {
    banda: banda,
    genero: genero,
    canciones: canciones
}
console.log(metallicaV);

// Forma moderna
const metallica = {banda, genero, canciones};
console.log(metallica);