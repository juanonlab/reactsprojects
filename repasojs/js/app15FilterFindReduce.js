// Métodos en arreglos

const personas = [
    {nombre: 'Juan', edad: 40, aprendiendo: 'Javascript'},
    {nombre: 'Vir', edad: 42, aprendiendo: 'Diseño'},
    {nombre: 'Chelo', edad: 66, aprendiendo: 'Aeronautica'},
    {nombre: 'Amalia', edad: 7, aprendiendo: 'Matemáticas'},
    {nombre: 'Eduardo', edad: 34, aprendiendo: 'Latín'},
];

console.log(personas);

// USO DE FILTER
// Mayores 28 años
const mayores = personas.filter(persona => {
    return persona.edad > 28;
})
console.log(mayores);

// USO DE FIND
// Que aprende amalia y su edad
const buscaAmalia = personas.find(persona => {
    return persona.nombre == 'Amalia';
})
console.log(`Amalia está aprendiendo ${buscaAmalia.aprendiendo} y tiene ${buscaAmalia.edad} años`);

// USO DE REDUCE
// Suma de los años
let total = personas.reduce((edadTotal, persona) => {
    return edadTotal + persona.edad;
}, 0);

console.log(total);

//Contar nombres en un array
const nombres = [
'Pedro','Manuel','Manoli','Luisa','Manuel','Pedro','Juan','Pedro'
];

const cantidadNombres = nombres.reduce((contadorNombre, nombre) => {
  contadorNombre[nombre] = (contadorNombre[nombre] || 0) + 1;
  return contadorNombre;
},{}
);
console.log(cantidadNombres);

// Explicacion:
// Primera ejecución
// 1.- contadorNombre = {} // Objecto vacío
// 2.- nombre = Jorge // Primer elemento del array
// 3.- {Jorge: 1}
// 4.- Se retorna el objecto existen -> {Jorge: 1}
// 5.- contadorNombre = {Jorge: 1} // Se repite el ciclo
