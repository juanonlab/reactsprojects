// Métodos o funciones en un objeto

// This para acceder a variables del objeto OBLIGATORIO!!
const persona = {
    nombre: 'Juan',
    trabajo: 'Cantante',
    edad: 40,
    mostrarInformacion: function() {
        console.log(`${this.nombre} es ${this.trabajo} y tiene ${this.edad} años.`)
    }
}

persona.mostrarInformacion();