import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import registerServiceWorker from './registerServiceWorker';

// React sin JSX
/*const titulo = React.createElement(
    'h1',
    {id: 'titulo', className: 'encabezado'},
    'Hola Mundo'
);*/

// Que, donde
/* ReactDOM.render(titulo, document.getElementById('root')); */
ReactDOM.render(<App />, document.getElementById('root'));
registerServiceWorker();
