import React, {
  Component
} from 'react';
import './App.css';

class App extends Component {
  render() {
    const empleado = {
      nombre: 'Juan',
      trabajo: 'Desarrolador Big Data'
    }
    return (
      // Usar className en vez de class para las clases de los divs
      <React.Fragment>
        <h1 className="mayusculas">Detalles empleado</h1>
        <p>Nombre: {empleado.nombre}</p>
        <p>Trabajo {empleado.trabajo} </p>
      </React.Fragment>
    );
  }
}

export default App;